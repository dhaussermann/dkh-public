(function(){

        'use strict';
    document.getElementById('convert').addEventListener('submit', function(event){
        event.preventDefault();
        const distance = parseFloat(document.getElementById('distance').value);
        const answer = document.getElementById('answer');


        if (distance) {
        const conversion = Number((distance * 1.609344).toFixed(3));
        //conversion = Number(conversion.toFixed(3));
        //alert(conversion);

        answer.innerHTML=`<h2>${distance} miles is equal to ${conversion} KM.</h2>`;

        }
        else{
            //Display error
            answer.innerHTML='<h2>Please provide a number.</h2>';
            // Add a comment on line 21
        
            

        }

    });
})()
